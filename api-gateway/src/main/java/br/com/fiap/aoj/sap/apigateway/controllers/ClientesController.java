package br.com.fiap.aoj.sap.apigateway.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.fiap.aoj.sap.apigateway.dtos.Cliente;

@RequestMapping(path = "/clientes")
public interface ClientesController {
	
	@GetMapping(path = "/")
	List<Cliente> getClientes();
	
}
