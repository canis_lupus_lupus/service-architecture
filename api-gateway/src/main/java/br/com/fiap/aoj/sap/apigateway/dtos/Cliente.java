package br.com.fiap.aoj.sap.apigateway.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Cliente {
	private String nome;
	private String email;
	private String endereco;
}
