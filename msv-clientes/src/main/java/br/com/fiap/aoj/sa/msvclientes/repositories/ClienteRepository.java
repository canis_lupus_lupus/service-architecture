package br.com.fiap.aoj.sa.msvclientes.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.fiap.aoj.sa.msvclientes.entities.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
	
	List<Cliente> findByNome(String nome);
	
	List<Cliente> findByEmail(String nome);

	List<Cliente> findByEndereco(String nome);
	

}
