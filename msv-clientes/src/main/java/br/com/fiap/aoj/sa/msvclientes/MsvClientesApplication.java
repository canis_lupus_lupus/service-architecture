package br.com.fiap.aoj.sa.msvclientes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.config.EnableHypermediaSupport.HypermediaType;

@SpringBootApplication
@EnableEurekaClient
@EnableHypermediaSupport(type = HypermediaType.HAL)
public class MsvClientesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsvClientesApplication.class, args);
	}

}
