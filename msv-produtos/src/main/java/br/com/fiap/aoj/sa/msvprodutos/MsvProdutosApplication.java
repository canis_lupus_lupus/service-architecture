package br.com.fiap.aoj.sa.msvprodutos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class MsvProdutosApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsvProdutosApplication.class, args);
	}

}
