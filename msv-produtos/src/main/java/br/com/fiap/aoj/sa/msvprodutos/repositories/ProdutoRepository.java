package br.com.fiap.aoj.sa.msvprodutos.repositories;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.fiap.aoj.sa.msvprodutos.entities.Produto;

@RepositoryRestResource(collectionResourceRel = "produtos", path = "produtos")
public interface ProdutoRepository extends JpaRepository<Produto, Long>{
	
	List<Produto> findByPreco(BigDecimal preco);
	
	List<Produto> findByNome(String nome);
	
}
